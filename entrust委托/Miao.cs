﻿using System;
using System.Collections.Generic;
using System.Text;

namespace entrust委托
{
    /// <summary>
    /// 猫类
    /// </summary>
    public class Miao
    {
        /// <summary>
        /// 定义一个委托 无参无返回值
        /// </summary>
        public delegate void MyMaoDeom();

        /// <summary>
        /// 事件 委托和事件的区别就在，事件是委托的实例
        /// </summary>
        public event MyMaoDeom MyMaoDeomEvent;

        /// <summary>
        /// 猫叫了发生动作
        /// </summary>
        public void Call()
        {
            Console.WriteLine("猫叫了发生动作");
            Movement movement = new Movement();
            movement.Fly();
            movement.Run();
            movement.Swimming();
        }

        /// <summary>
        /// 猫叫了发生动作
        /// </summary>
        public void CallNew()
        {
            Console.WriteLine("猫叫了发生动作");
            Movement movement = new Movement();
            if (MyMaoDeomEvent != null)
            {
                MyMaoDeomEvent.Invoke();
            }
        }
    }


    public class Movement
    {
        /// <summary>
        /// 飞
        /// </summary>
        public void Fly()
        {
            Console.WriteLine("飞");
        }

        /// <summary>
        /// 跑
        /// </summary>
        public void Run()
        {
            Console.WriteLine("跑");
        }

        /// <summary>
        /// 游泳
        /// </summary>
        public void Swimming()
        {
            Console.WriteLine("游泳");
        }
    }

}
