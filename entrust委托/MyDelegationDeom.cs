﻿using System;
using System.Collections.Generic;
using System.Text;

namespace entrust委托
{
    /// <summary>
    /// 委托
    /// </summary>
    //定义来了一个全局的委托 无参无返回值
    //特点就是在本类的所有方法调用
    public delegate void MyDelegate();

    /// <summary>
    /// 1:这里的无参，无返回值，表示传递的方法是一个没有参数，没有返回值的方法。
    /// 2:委托就是一个类，别把它想成了方法，所以不能重载。委托也不能继承因为是密封类。
    /// 3:不要在方法使用委托，委托在传递此方法。
    /// </summary>
    public class MyDelegationDeom
    {

        /// <summary>
        /// 无参无返回值
        /// </summary>
        public delegate void MyDelegate();

        /// <summary>
        /// 有参无返回值
        /// </summary>
        public delegate void MyDelegat1(int x);

        /// <summary>
        /// 有参有返回值
        /// </summary>
        public delegate int MyDelegate2(int x);

        /// <summary>
        /// 无参有返回值
        /// </summary>
        public delegate int MyDelegate3();

        /// <summary>
        /// 泛型委托
        /// </summary>
        /// <typeparam name="T">类型</typeparam>
        /// <param name="t"></param>
        public delegate void MyDelegate<T>(T t);

        /// <summary>
        /// 方法
        /// </summary>
        public void Show()
        {
            //MyDelegate myDelegate = new MyDelegate(Show);
            //myDelegate.Invoke();
            Console.WriteLine("Hello World!");
        }

    }
}
