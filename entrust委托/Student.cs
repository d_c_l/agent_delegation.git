﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace entrust委托
{
    /// <summary>
    /// 学生类
    /// </summary>
    public class Student
    {
        /// <summary>
        /// 主键
        /// </summary>
        public int ID { get; set; }
        
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 成绩
        /// </summary>
        public int Score { get; set; }
    }
}
